import Vue from 'vue'
import VueRouter from 'vue-router'
import PageOne from '../views/page-1.vue'
import PageTwo from '../views/page-2.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '',
    redirect: '/page1'
  },
  {
    path: '/page1',
    name: 'page1',
    component: PageOne
  },
  {
    path: '/page2',
    name: 'page2',
    component: PageTwo
  }
]

const router = new VueRouter({
  routes,
  mode: 'history'
})

export default router
