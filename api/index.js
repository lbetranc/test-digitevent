const express = require('express')
const app = express()
var cors = require('cors')
const port = 8000;
const axios = require('axios');

app.use(express.json())
app.use(cors())

// GET /date?dates[]=dd/mm 
// Return array [{date, ans}, {}..]
app.get('/date/', async (req, res) => {
    let dates = req.query.dates;
    let requests = [];
    var array = [];

    dates.forEach(function(item) {
        let url = 'http://numbersapi.com/'+item+'/date';
        requests.push(axios.get(url))
    })
    
    let answers = await requete(requests);

    dates.forEach(function(item, index) {
        array[index] = {date: item, ans: answers[index]};
    });

    res.status(200).send(array)
})

const requete = async (requests) => {
    return new Promise((resolve) => {
        axios.all(requests)
        .then(responseArr => {
            var dataArray = responseArr.map((item) => 
                item["data"]
            );
            resolve(dataArray)
        })
        .catch(e => {
            this.errors = e
        })
    })
}

app.listen(port, () => {
    console.log('Server app listening on port ' + port);
});
